pragma solidity ^0.4.11;

contract EqualPay {
    
    uint totalReceived = 0;
    uint totalEmployees = 0;
    
    //address[5] employees;
    mapping(uint => address) employees; 
    
    modifier companySizeLimit() { //first five
        require(totalEmployees < 5);
        _;
    }
    
    modifier belongsToCompany() {
        require(employeeExist());
        _;
    }
    
    modifier createIfNotExist() {
        if (!employeeExist()) {
            employees[totalEmployees] = msg.sender;
            totalEmployees += 1;
        }
        _;
    }
    
    function deposit() public payable companySizeLimit createIfNotExist returns (uint) {
        totalReceived += msg.value;
        return totalReceived;
    }

    
    function cashOut() public payable canCashOut {
        if (totalReceived > 0) {
            uint totalWithdraw = totalReceived/totalEmployees;
            msg.sender.transfer(totalWithdraw);
            totalReceived = totalReceived - totalWithdraw;
        }
    }
    
    function total() public view belongsToCompany returns(uint) {
        return totalReceived;
    }
    
    function employeeExist() internal view returns (bool) {
        bool inCompany = false;
       
        for (uint i = 0; i < totalEmployees; i++) {
            if (employees[i] == msg.sender) inCompany = true;
        }
        
        return inCompany;
    }
    
    
    modifier canCashOut() {
        require(employeeExist());
        _;
    }
    
}